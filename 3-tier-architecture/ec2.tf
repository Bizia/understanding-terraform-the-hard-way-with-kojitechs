
################################################################################
# CREATING AN EC2 INSTANCE USING COUNT 
################################################################################

data "aws_ami" "ami" {
  most_recent      = true
  owners           = ["amazon"] 

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}


################################################################################
# CREATING AN EC2 INSTANCE USING COUNT 
################################################################################

resource "aws_instance" "this" {
  ami           = data.aws_ami.ami.id 
  instance_type = var.instance_type
  subnet_id      = aws_subnet.private_subnet[0].id
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
}
