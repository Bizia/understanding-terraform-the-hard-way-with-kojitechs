variable "vpc_cidr" {
  type        = string
  description = "The value for the vpc cidr"
}

variable "component" {
  type        = string
  description = "Name of the project we are working on"
  default     = "3-tier-architecture"
}

variable "public_subnetcidr" {
  type        = list(any)
  description = "(optional) describe your variable"
}

variable "private_subnetcidr" {
  type        = list(any)
  description = "(optional) describe your variable"
}

variable "database_subnetcidr" {
  type        = list(any)
  description = "(optional) describe your variable"
}

variable "instance_type" {
  type = string
  description = "Instance type for ecs instances"
}

