# Terraform Settings Block
terraform {
  required_version = "1.2.6"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" # Optional but recommended in production
    }
  }
}


# Provider Block
provider "aws" {
  profile = "default" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}


# Resource Block
resource "aws_instance" "ec2_instance" {
  ami           = "ami-04d29b6f966df1537" # Amazon Linux in us-east-1, update as per your region
  instance_type = "t2.micro"
  tags = {
    Name ="ec2_instance"
  }
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

# variable block

variable "ami_id" {
  type = string
  description = "ami id"
  default = "ami-04d29b6f966df1537"

  
}